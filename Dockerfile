#
#   CIAO Application Dockerfile
#

# app/dbio/base is based on Ubuntu 18.04 LTS
# Please check for details: https://docs.databiology.net/tiki-index.php?page=Application%2BBase%2BImages
FROM app/dbio/base:4.2.2

## replace with contact details if wanted, but CIAO will project it:
# LABEL maintainer "FirstName LastName <name@example.com>"


 
## add required ubuntu packages, eg htop and curl:
# RUN    apt-get update -q=2 \
#    && apt-get upgrade -q=2 \
#    && apt-get install -yq=2 --no-install-recommends \
#        htop curl \
#    && apt-get clean \
#    && apt-get purge \
#    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*



## copy main application script:
# COPY path/to/script     /usr/local/bin/main.sh
# RUN  chmod +x           /usr/local/bin/main.sh

